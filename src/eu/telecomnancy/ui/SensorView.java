package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.Commande;
import eu.telecomnancy.sensor.CommandeOff;
import eu.telecomnancy.sensor.CommandeOn;
import eu.telecomnancy.sensor.CommandeUpdate;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class SensorView extends JPanel implements MyObserver {
    private AbstractSensor sensor;
    private ArrayList<Commande> alc;
    

    private JLabel value = new JLabel("");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(AbstractSensor c) {
    	this.alc = new ArrayList<Commande>();
        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	Commande c = new CommandeOn(sensor);
            	alc.add(c);
                c.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	Commande c = new CommandeOff(sensor);
            	alc.add(c);
                c.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	if (sensor.getStatus()) {
                	Commande c = new CommandeUpdate(sensor);
                	alc.add(c);
	                c.execute();
            	}
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
        this.refresh();
        
        c.addObserver(this);
    }

	@Override
	public void refresh() {
		if (this.sensor.getStatus()) {
			on.setEnabled(false);
			off.setEnabled(true);
			update.setEnabled(true);
			
			try {
				this.value.setText("Value : "+round(this.sensor.getValue(), 2)+"� ");
			} catch (SensorNotActivatedException e) {
				e.printStackTrace();
			}
		}
		else {
			on.setEnabled(true);
			off.setEnabled(false);
			update.setEnabled(false);
			this.value.setText("N/A");
		}
	}
	
	static public double round(double value, int n) {
		double r = (Math.round(value * Math.pow(10, n))) / (Math.pow(10, n));
		return r;
	}
}
