package eu.telecomnancy.ui;

import java.util.ArrayList;

public abstract class MyObservable {
	
	protected ArrayList<MyObserver> observersList = new ArrayList<MyObserver>();

	public void addObserver(MyObserver o) {
		this.observersList.add(o);
	}

	
	public void removeObserver(MyObserver o) {
		this.observersList.remove(o);		
	}

	public void notifyObservers() {
		for (MyObserver o : this.observersList)
			o.refresh();
		
	}


}
