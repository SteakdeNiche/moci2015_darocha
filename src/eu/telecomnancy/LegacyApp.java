package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.AdapterLegacyTempSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class LegacyApp {

    public static void main(String[] args) {
        ISensor sensor = new AdapterLegacyTempSensor();
        new ConsoleUI(sensor);
    }

}