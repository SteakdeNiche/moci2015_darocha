package eu.telecomnancy.sensor;

public abstract class SensorState extends AbstractSensor {
	
	protected StateTemperatureSensor stateTempSens;
	
	protected SensorState(StateTemperatureSensor s) {
		super();
		this.stateTempSens  = s;
	}
	
	@Override
	public abstract void on();
	
	@Override
	public abstract void off();
	
	@Override
	public abstract void update() throws SensorNotActivatedException;
	
	@Override
	public abstract double getValue() throws SensorNotActivatedException;
	
	@Override
	public abstract boolean getStatus();
	
}
