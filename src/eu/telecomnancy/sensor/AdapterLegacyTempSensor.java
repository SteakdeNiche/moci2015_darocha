package eu.telecomnancy.sensor;


public class AdapterLegacyTempSensor extends AbstractSensor {

	private LegacyTemperatureSensor lts;
	
	public AdapterLegacyTempSensor() {
		this.lts = new LegacyTemperatureSensor();
	}

	@Override
	public void on() {
		if (!this.lts.getStatus())
			this.lts.onOff();
		notifyObservers();
	}

	@Override
	public void off() {
		if (this.lts.getStatus())
			this.lts.onOff();
		notifyObservers();
	}

	@Override
	public boolean getStatus() {
		return this.lts.getStatus();
	}


	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.lts.getTemperature();
	}
	
	@Override
	public void update() throws SensorNotActivatedException {
		if (this.lts.getStatus()) {
			this.lts.onOff();
			this.lts.onOff();
		}
		else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
		notifyObservers();
	}

	
}
