package eu.telecomnancy.sensor;

public class CommandeUpdate extends Commande {

	public CommandeUpdate(AbstractSensor abse) {
		super(abse);
	}

	@Override
	public void execute() {
		//On lance la fonction update
		try {
			this.as.update();
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
		
	}

}
