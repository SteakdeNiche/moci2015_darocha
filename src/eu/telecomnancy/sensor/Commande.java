package eu.telecomnancy.sensor;

public abstract class Commande {
	
	protected AbstractSensor as;
	
	public Commande (AbstractSensor abse){
		this.as=abse;
	}
		
	/*
	 * L'interface n'a qu'une m�thode qui sera propre aux commandes. 
	 */
	public abstract void execute();
}
