package eu.telecomnancy.sensor;

public class DecoratorRoundSensor extends AbstractSensor {
	
	AbstractSensor sens;
	
	public DecoratorRoundSensor(AbstractSensor as){
		this.sens=as;
	}

	public void on() {
		this.sens.on();
		notifyObservers();
		
	}

	public void off() {
		this.sens.off();
		notifyObservers();
		
	}
	public boolean getStatus() {
		return this.sens.getStatus();
	}

	public double getValue() throws SensorNotActivatedException {
		return Math.round(this.sens.getValue());
	}

	public void update() throws SensorNotActivatedException {
		this.sens.update();
		notifyObservers();
		
	}

}
