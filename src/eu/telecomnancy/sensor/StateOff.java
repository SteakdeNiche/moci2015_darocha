package eu.telecomnancy.sensor;

public class StateOff extends SensorState {
	
	
	protected StateOff(StateTemperatureSensor s) {
		super(s);
	}

	public void on() {
		stateTempSens.setSensorState(new StateOn(stateTempSens));
	}
	
	public void off() {
	}
	
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
	
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
	
	public boolean getStatus() {
		return false;
	}

}