package eu.telecomnancy.sensor;

public class DecoratorFahrenheitSensor extends AbstractSensor{

	AbstractSensor sens;
	
	public DecoratorFahrenheitSensor(AbstractSensor as){
		this.sens=as;
	}
	public void on() {
		this.sens.on();
		notifyObservers();
		
	}

	@Override
	public void off() {
		this.sens.off();
		notifyObservers();
		
	}

	@Override
	public boolean getStatus() {
		return this.sens.getStatus();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.sens.getValue()*1.8 + 32;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.sens.update();
		notifyObservers();
		
	}

}
