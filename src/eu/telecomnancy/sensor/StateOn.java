package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn extends SensorState {
	
	
	protected StateOn(StateTemperatureSensor s) {
		super(s);
	}

	public void on() {
	}
	
	public void off() {
		stateTempSens.setSensorState( new StateOff(stateTempSens));
	}
	
	public void update() {
        stateTempSens.setVal((new Random()).nextDouble() * 100);
	}
	
	public double getValue() throws SensorNotActivatedException {
		return stateTempSens.getVal();
	}
	
	public boolean getStatus() {
		return true;
	}

}
