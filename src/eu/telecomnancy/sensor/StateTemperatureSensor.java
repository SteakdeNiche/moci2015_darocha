package eu.telecomnancy.sensor;


public class StateTemperatureSensor extends AbstractSensor {

    private double val = 0;
    private SensorState sensorState = new StateOff(this);
    
    public StateTemperatureSensor() {
    	val = 0;
    	sensorState = new StateOff(this);
    }
   
    
    public double getVal() {
		return val;
	}

	public void setVal(double val) {
		this.val = val;
	}

	public void setSensorState(SensorState s) {
    	sensorState = s;
    }
    
    public SensorState getSensorState() {
    	return sensorState;
    }
    
	@Override
	public void on() {
		sensorState.on();
		notifyObservers();
	}
	@Override
	public void off() {
		sensorState.off();
		notifyObservers();
	}
	@Override
	public boolean getStatus() {
		return sensorState.getStatus();
	}
	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensorState.getValue();
	}
	@Override
	public void update() throws SensorNotActivatedException {
		sensorState.update();
		notifyObservers();
	}
    


    
	
}
