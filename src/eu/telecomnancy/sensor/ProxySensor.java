package eu.telecomnancy.sensor;

import java.util.Date;

public class ProxySensor extends AbstractSensor {
	
	AbstractSensor sens;
	
	public ProxySensor(AbstractSensor as){
		this.sens=as;
	}

	@Override
	public void on() {
		this.sens.on();
		System.out.println(new Date()+" | On method : "+this.sens.getStatus());
		notifyObservers();
		
	}

	@Override
	public void off() {
		this.sens.off();
		System.out.println(new Date()+" | Off method : "+this.sens.getStatus());
		notifyObservers();
		
	}

	@Override
	public boolean getStatus() {
		boolean gs=this.sens.getStatus();
		System.out.println(new Date()+" | getStatus method : "+gs);
		return gs;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		System.out.println(new Date()+" | getValue method : " + this.sens.getValue());
		return this.sens.getValue();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		System.out.println(new Date()+" ["
				+ "| update method : " + this.sens.getStatus());
		this.sens.update();
		notifyObservers();
		
	}

}
