package eu.telecomnancy.sensor;

public class Fabrique {

	public AbstractSensor make(String type, String opt){

		switch (opt){
		case "F": //option Fahrenheit
			switch (type){
			case "leg" :
				AdapterLegacyTempSensor lts = new AdapterLegacyTempSensor();
				return new DecoratorFahrenheitSensor(lts);
			case "state" :
				StateTemperatureSensor sts = new StateTemperatureSensor();
				return new DecoratorFahrenheitSensor(sts);
			case "R" : 
				DecoratorRoundSensor drs = new DecoratorRoundSensor(new TemperatureSensor());
				return new DecoratorFahrenheitSensor(drs);
			case "proxy" : 
				ProxySensor ps = new ProxySensor(new TemperatureSensor());
				return new DecoratorFahrenheitSensor(ps);
			default: //Un simple fahrenheit
				return new DecoratorFahrenheitSensor(new TemperatureSensor());
			}
		case "R": //option Arrondi
			switch (type){
			case "leg" :
				AdapterLegacyTempSensor llts = new AdapterLegacyTempSensor();
				return new DecoratorRoundSensor(llts);
			case "state" :
				StateTemperatureSensor ssts = new StateTemperatureSensor();
				return new DecoratorRoundSensor(ssts);
			case "F" : 
				DecoratorFahrenheitSensor dfs = new DecoratorFahrenheitSensor(new TemperatureSensor());
				return new DecoratorRoundSensor(dfs);
			case "proxy":
				ProxySensor pps = new ProxySensor(new TemperatureSensor());
				return new DecoratorRoundSensor(pps);
			default: //un simple round
				return new DecoratorRoundSensor(new TemperatureSensor());
			}
		default : 
			switch (type){
			case "leg" :
				return new AdapterLegacyTempSensor();
			case "state" :
				return new StateTemperatureSensor();
			case "F" : 
				return new DecoratorFahrenheitSensor(new TemperatureSensor());
			case "R" :
				return new DecoratorRoundSensor(new TemperatureSensor());
			case "proxy":
				ProxySensor pps = new ProxySensor(new TemperatureSensor());
				return new DecoratorFahrenheitSensor(pps);
			default: //un simple round
				return new DecoratorFahrenheitSensor(new TemperatureSensor());

			}

		}
	}
}
