package eu.telecomnancy;

import eu.telecomnancy.sensor.AdapterLegacyTempSensor;
import eu.telecomnancy.sensor.DecoratorFahrenheitSensor;
import eu.telecomnancy.sensor.DecoratorRoundSensor;
import eu.telecomnancy.sensor.Fabrique;
import eu.telecomnancy.sensor.ProxySensor;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.StateTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
    	Fabrique factory = new Fabrique();
    	//Pour l'instanciation on choisit � gauche le type de sensor, et � droite l'option R pour round et F pour fahrenheit
    	//Toutes les possibilit�s sont pr�cis�es dans fabrique
    	AbstractSensor sensor = factory.make("legacy", "R");
    //	AbstractSensor sensor = new DecoratorFahrenheitSensor(new TemperatureSensor());
    //	AbstractSensor sensor = new ProxySensor(new TemperatureSensor());
   // 	AbstractSensor sensor = new StateTemperatureSensor();
//    	AbstractSensor sensor = new TemperatureSensor();
//      AbstractSensor sensor = new AdapterLegacyTempSensor();
        new MainWindow(sensor);
    }

}
